#! /bin/sh
# Fill the contextual menu tclplugin : "tclplugins/malinette-menu"
# Take the list of objects from malinette-objects-list.txt

# File input
fileobjects="../preferences/malinette-objects-list.txt"
#fileobjects="../preferences/brutbox-objects-list.txt"

# File output
filemenu="tclplugins/malinette-menu/menu_tree.txt"

# Write into file
echo -n "" > $filemenu

IFS="
"

for line in $(cat "$fileobjects")
do	
	# Find category
	category=$(echo $line | cut -d ":" -f1)

	# Avoid the master category
	if [ $category != "master" ]; then
		echo -n "{ $category {" >> $filemenu

		objects=$(echo $line | cut -d ":" -f2 | cut -d ";" -f1)
		IFS=" "
		for object in $objects
		do
			patch="../abs/malinette-abs/$object-help.pd"

			# Special case with [average] which is not in the Malinette
			if [ $object = "average" ]; then
				description="Average of last N values (maxlib)"
			else
				description=$(cat $patch | tr "\n" " " | sed -nr 's/.*#X text .* \\; -----* \\;(.*)/\1/p' | cut -d ";" -f1 | sed 's/\\//')
			fi

			# Add to tclplugins, avoid "master" objects
			echo -n "$object " >> $filemenu

		done

		echo "}}" >> $filemenu

	fi
done