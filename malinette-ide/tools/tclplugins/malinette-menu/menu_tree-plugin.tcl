# META NAME menu tree (custom)
# META DESCRIPTION create your own menu with right clic, 2 levels custom
# META AUTHOR ? + customs Jerome Abel
# META VERSION ?
# META LICENSE public domain

package require pd_menus

namespace eval category_menu {
}

proc category_menu::load_menutree {} {
    # load object -> tags mapping from file in Pd's path
    set testfile [file join $::current_plugin_loadpath menu_tree.txt]
    set f [open $testfile]
    set menutree [read $f]
    #set menutree [regsub -all {([\{\s])\-([\s\}])} [read $f] {\1\\\\-\2}]
    close $f
    unset f
    return $menutree
}

proc category_menu::create {mymenu} {
    set menutree [load_menutree]
    $mymenu add separator
    foreach categorylist $menutree {
        set category [lindex $categorylist 0]
        menu $mymenu.$category
        $mymenu add cascade -label [_ [string totitle $category]] -menu $mymenu.$category
        foreach item [lindex $categorylist end] {
            $mymenu.$category add command \
                    -label [regsub -all {^\-$} $item {?}] \
                    -font "courrier 8 normal" \
                    -command \
                    "pdsend \"\$::focused_window obj \$::popup_xcanvas \$::popup_ycanvas $item\""
        }
    }
}

category_menu::create .popup