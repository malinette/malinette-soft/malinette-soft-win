#! /bin/sh
# Reset all sizes and positions patchs
# No argument = local malinette + linux sizes
# Argument : linux, win, mac to releases folders (in my computer: "../../malinette-soft")
# Usage example : ./resize_patches linux

# Test arguments
if [ -z "$1" ]
then
	OS="linux"
	DEST_FOLDER=".."
else
	OS="$1"
	DEST_FOLDER="../../malinette-soft/src/$OS/malinette-ide"
fi

# OS specific variables
# When the variable ends with "BB", it means for Brutbox
if [ "$OS" = "linux" ]
then
	SIZE_MENU="0 0 160 687"
	SIZE_EXAMPLES="162 0 790 687"
	SIZE_MENU_BB="1 107 185 647"
	SIZE_EXAMPLES_BB="187 79 933 677"
elif [ "$OS" = "mac" ]
then
	SIZE_MENU="0 0 160 687"
	SIZE_EXAMPLES="162 0 790 687"
	SIZE_MENU_BB="1 107 185 647"
	SIZE_EXAMPLES_BB="187 79 933 677"
elif [ "$OS" = "win" ]
then
	#27 23 150 687
	#178 23 800 687
	SIZE_MENU="0 0 160 687"
	SIZE_EXAMPLES="162 0 790 687"
	SIZE_MENU_BB="1 107 185 647"
	SIZE_EXAMPLES_BB="187 79 933 677"
else
	echo "Error, try those arguments : mac, win or linux"
	exit 1
fi

# Resize the menu MALINETTE.pd : sed replace the first line 
sed -i "1 s/^.*$/#N canvas $SIZE_MENU 10;/g" "$DEST_FOLDER/MALINETTE.pd"

# Resize the menu BRUTBOX.pd : sed replace the first line 
sed -i "1 s/^.*$/#N canvas $SIZE_MENU_BB 10;/g" "$DEST_FOLDER/BRUTBOX.pd"

# Resize examples, manuals and projects patches
for folder in $patches
do
IFS="
"
	for file in $(ls "$DEST_FOLDER/$folder/"*"/"*".pd")
	do   
		if [ $folder != ".*brutbox.*" ]
		then
			sed -i "1 s/^.*$/#N canvas $SIZE_EXAMPLES_BB 10;/g" $file
		else
			sed -i "1 s/^.*$/#N canvas $SIZE_EXAMPLES 10;/g" $file
		fi
	done
done