    README for pd external 'readdir'

    Last updated for readdir v0.02

DESCRIPTION
    The 'readdir' object lets you read the contents of a directory.

INSTALLATION
    Issue the following commands to the shell:

       cd readdir-X.YY  (or wherever you extracted the distribution)
       ./configure
       make
       make install

BUILD OPTIONS
    The 'configure' script supports the following options, among others:

    * --enable-debug , --disable-debug
        Whether to enable verbose debugging messages. Default=no.

ACKNOWLEDGEMENTS
    PD by Miller Puckette and others.

    Ideas, black magic, and other nuggets of information drawn from code by
    Guenter Geiger, Larry Troxler, and iohannes m zmoelnig.

KNOWN BUGS
    None known.

AUTHOR
    Bryan Jurish <moocow@ling.uni-potsdam.de>

/////////////////////////////////////////

    README for pd external package 'pdstring'

    Last updated for pdstring v0.06

DESCRIPTION
    The 'pdstring' package contains objects for converting to and from
    (ASCII)-strings, represented as lists of floats.

INSTALLATION
    Issue the following commands to the shell:

       cd PACKAGE-XX.YY  (or wherever you extracted the distribution)
       ./configure
       make
       make install

BUILD OPTIONS
    The 'configure' script supports the following options, among others:

    * --help
        Output a brief usage summary of the 'configure' script, including a
        list of supported options and influential environment variables.

    * --enable-debug , --disable-debug
        Whether to enable verbose debugging messages. Default=no.

ACKNOWLEDGEMENTS
    PD by Miller Puckette and others.

    Ideas, black magic, and other nuggets of information drawn from code by
    Guenter Geiger, Larry Troxler, and iohannes m zmoelnig.

KNOWN BUGS
  Memory Usage
    Encoding each byte of a string as its own float is shamefully wasteful:
    it uses only 1 byte out of at least 3 which could be losslessly used
    given ANSI/IEEE Std 754-1985 floats, not to mention the remaining
    byte(s) (usually 1) of the float itself or the (usually 4) bytes used
    for the a_type flag. Unfortunately, Pd trims some floating point
    precision in message boxes and in float atoms, so a truly lossless float
    encoding for Pd would only be possible using 2 bytes per float (wasting
    1/2 the space of the float itself), and (to me), the memory saving such
    an encoding would provide is just not worth the lack of transparency and
    additional workload it would involve (but contact me if you want the
    code anyways).

AUTHOR
    Bryan Jurish <moocow@ling.uni-potsdam.de>

/////////////////////////////////////

README for pd external 'sprinkler' (formerly 'forward')

DESCRIPTION
    'sprinkler' objects do dynamic control-message dissemination.

    Given a list as input, a 'sprinkler' object interprets the initial list
    element as the name of a 'receive' object, and [send]s the rest of the
    list to that object.

INSTALLATION
    Issue the following commands to the shell:

       cd sprinkler-X.YY  (or wherever you extracted the distribution)
       ./configure
       make
       make install

BUILD OPTIONS
    The 'configure' script supports the following options, among others:

    --enable-debug , --disable-debug
        Whether to enable verbose debugging messages. Default=no.

    --enable-forward , --disable-forward
        Whether to create [forward] objects as instances of the [sprinkler]
        class (MAX-incompatible). Default=no.

    --enable-all-forwardmess , --disable-all-forwardmess
        Whether to use pd_forwardmess() for all messages. If this option is
        disabled (the default), messages of length 1 will be handled
        specially; thus a symbol 'foo' will be passed as 'symbol foo',
        rather than just 'foo'.

        Default=no.

        Future versions of 'sprinkler' may use pd_forwardmess() for all
        messages by default -- go on, try it!

ACKNOWLEDGEMENTS
    PD by Miller Puckette and others.

    Ideas, black magic, and other nuggets of information drawn from code by
    Guenter Geiger, Larry Troxler, and iohannes m zmoeling.

    Thanks to Krzysztof Czaja for pointing out to me the existence of MAX
    "forward", and to Miller Puckette for the name "sprinkler".

    Thanks to Erasmus Zipfel for a bugreport and useful ideas.

KNOWN BUGS
    One of the acknowledgements used to be in this section. Sorry, folks.

    Backwards-compatible version is incompatible with MAX.

    Semantic strangeness with singleton messages is somewhat cryptic.

AUTHOR
    Bryan Jurish <moocow@ling.uni-potsdam.de>
	
////////////////////////////////////

README for weightmap

    Last updated for weightmap v0.02

DESCRIPTION
    weightmap is a PD external which maps incoming probability values to
    integers, inspired in part by Yves Degoyon's 'probalizer' object.

INSTALLATION
    Issue the following commands to the shell:

       cd weightmap-X.YY  (or wherever you extracted the distribution)
       ./configure
       make
       make install

ACKNOWLEDGEMENTS
    PD by Miller Puckette and others.

    probalizer object by Yves Degoyon.

    Ideas, black magic, and other nuggets of information drawn from code by
    Guenter Geiger, Larry Troxler, and iohannes m zmoelnig.

BUGS
    It's a misleading name: higher input "weights" don't neccesarily
    correspond to higer stored "weights".

    Probably many more serious ones as well.

AUTHOR
    Bryan Jurish <moocow@ling.uni-potsdam.de>

///////////////////////////////////////////

README for pd external 'deque'

    Last updated for version 0.03.

DESCRIPTION
    Double-ended message-queue for pd.

PLATFORMS
    * linux/x86
        This is what I run, so things really ought to work here.

    * Other Platforms
        See REQUIREMENTS, below.

REQUIREMENTS
    In order to build the "deque" library, you will need the following:

    * A C compiler
        Tested with gcc-3.3.3 under linux/x86 (Debian).

    * /bin/sh , sed
        In order to run the 'configure' script.

    * A make program
        Tested with GNU make v3.79.1 under linux/x86 (Debian).

    * PD
        Tested with pd v0.37.1 under linux/x86 (Debian). PD is available
        from:

         http://www.crca.ucsd.edu/~msp/software.html

INSTALLATION
    Issue the following commands to the shell:

       cd PACKAGENAME-X.YY  (or wherever you extracted the distribution)
       ./configure
       make
       make install

BUILD OPTIONS
    The 'configure' script supports the following options, among others:

    * --with-pd-dir=DIR
        PD base directory.

    * --with-pd-include=DIR
        Directory where the PD include files live.

    * --with-pd-extdir=DIR
        Where to put the externals on 'make install'.

    * --enable-debug , --disable-debug
        Whether to enable verbose debugging messages and code. Default=no.

ACKNOWLEDGEMENTS
    PD by Miller Puckette and others.

    Ideas, black magic, and other nuggets of information drawn from code by
    Guenter Geiger, Larry Troxler, and IOhannes m Zmoelnig.

KNOWN BUGS
    * General
        Only tested under linux.

AUTHOR / MAINTAINER
    Bryan Jurish <moocow@ling.uni-potsdam.de>

////////////////////////////////////////////



