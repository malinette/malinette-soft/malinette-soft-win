# malinette-soft
Open Source Kit For Programming Interactivity. 

## Description
Standalone version of the [malinette](http://malinette.info) project. It contains Pure Data, 21 libraries, malinette-ide and a startup script. To stay up-to-date, you can replace the [malinette-ide](https://framagit.org/malinette/malinette-ide) folder.

## Installation
- Download
- Extract
- Launch "start-malinette.bat" script

If nothing happens, try to put malinette-soft-win folder on your desktop.

## Launcher
You can add an alias or a launcher to make easiest the way to launch La Malinette. On Windows, right click on desktop and create an alias of start-malinette.bat.