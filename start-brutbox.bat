@ECHO OFF

set malinette=%cd%

set patch="BRUTBOX.pd"

"%malinette%/pd/bin/pd.exe" -helppath %malinette%/pd/doc/5.reference -font-weight normal -midiindev 1 -open %malinette%/malinette-ide/%patch% -path %malinette%/pd/extra -path %malinette%/externals -path %malinette%/malinette-ide/abs/malinette-abs -path %malinette%/malinette-ide/abs/brutbox -path %malinette%/externals/bassemu~ -path %malinette%/externals/comport -path %malinette%/externals/creb -path %malinette%/externals/cyclone -path %malinette%/externals/ext13 -path %malinette%/externals/ggee -path %malinette%/externals/hcs -path %malinette%/externals/iemguts -path %malinette%/externals/iemlib -path %malinette%/externals/list-abs -path %malinette%/externals/pduino -path %malinette%/externals/mapping -path %malinette%/externals/maxlib -path %malinette%/externals/moocow -path %malinette%/externals/pmpd -path %malinette%/externals/puremapping -path %malinette%/externals/purepd -path %malinette%/externals/sigpack -path %malinette%/externals/tof -path %malinette%/externals/zexy -path %malinette%/externals/completion-plugin -lib zexy -lib iemlib
